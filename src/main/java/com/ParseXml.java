package com;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ParseXml {

    public static void main(String[] args) throws IOException {
        final String fileName = "address.xml";

        try (ParseStreamProcessor processor = new ParseStreamProcessor(Files.newInputStream(Paths.get("address.xml")))) {
            XMLStreamReader reader = processor.getReader();
            while (reader.hasNext()) {
                int event = reader.next();
                if (event == XMLEvent.START_ELEMENT &&
                        "city".equals(reader.getLocalName())) {
                    System.out.println(reader.getElementText());
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}